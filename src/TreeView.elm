module TreeView exposing (Node, TreeView(..), init, renderNode, renderTreeView, view)

import Html exposing (..)
import Html.Attributes as HA
import Html.Events exposing (..)


type TreeView
    = RootNode Node (List TreeView)


type alias Node =
    { id : Int
    , text : String
    }


init : TreeView
init =
    RootNode (Node 0 "Comp list")
        [ RootNode (Node 1 "Ambulance") []
        , RootNode (Node 2 "Extras")
            [ RootNode (Node 20 "Dental") []
            , RootNode (Node 21 "Aids") []
            , RootNode (Node 22 "Maintenance") []
            ]
        , RootNode (Node 3 "Hospital") []
        ]


view : TreeView -> Html msg
view treeView =
    renderTreeView treeView


renderTreeView : TreeView -> Html msg
renderTreeView treeView =
    case treeView of
        RootNode node treeViewList ->
            div [ HA.class "root" ] [ renderNode node treeViewList ]


renderNode : Node -> List TreeView -> Html msg
renderNode node treeViewList =
    let
        nodeText =
            "(" ++ String.fromInt node.id ++ ") " ++ node.text |> text
    in
    div [ HA.class "node" ]
        (nodeText
            :: List.map renderTreeView treeViewList
        )
