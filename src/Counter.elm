module Counter exposing (Counter, Msg, makeCounter, renderCounter, update)

import Html exposing (..)
import Html.Events exposing (..)


type Counter
    = Counter Int


type Msg
    = Incr
    | Decr


makeCounter =
    Counter 0


renderCounter : Counter -> Html Msg
renderCounter (Counter value) =
    div []
        [ button [ onClick Incr ] [ text "+" ]
        , text (String.fromInt value)
        , button [ onClick Decr ] [ text "-" ]
        ]


update : Msg -> Counter -> Counter
update msg (Counter value) =
    case msg of
        Incr ->
            Counter (value + 1)

        Decr ->
            Counter (value - 1)
