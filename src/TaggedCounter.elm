module TaggedCounter exposing (Counter, Msg, makeCounter, renderCounter, update)

import Html exposing (..)
import Html.Events exposing (..)


type Counter
    = Counter CounterValue


type CounterValue
    = CounterValue Int


type Msg
    = Incr
    | Decr


makeCounter =
    Counter (CounterValue 0)


renderCounter : Counter -> Html Msg
renderCounter (Counter (CounterValue value)) =
    div []
        [ button [ onClick Incr ] [ text "+" ]
        , text (String.fromInt value)
        , button [ onClick Decr ] [ text "-" ]
        ]


update : Msg -> Counter -> Counter
update msg (Counter (CounterValue value)) =
    case msg of
        Incr ->
            Counter (CounterValue (value + 1))

        Decr ->
            Counter (CounterValue (value - 1))
