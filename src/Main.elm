module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Counter
import Html exposing (..)
import Html.Attributes exposing (src)
import TaggedCounter
import TreeView



---- MODEL ----


type alias Model =
    { counters : List Counter.Counter
    , taggedCounters : List TaggedCounter.Counter
    , treeView : TreeView.TreeView
    }


init : ( Model, Cmd Msg )
init =
    ( { counters = [ Counter.makeCounter, Counter.makeCounter ]
      , taggedCounters = [ TaggedCounter.makeCounter, TaggedCounter.makeCounter ]
      , treeView = TreeView.init
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | CounterMsg Int Counter.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        CounterMsg ix subMsg ->
            let
                newCounters =
                    List.indexedMap
                        (\currentCounter counter ->
                            if ix == currentCounter then
                                Counter.update subMsg counter

                            else
                                counter
                        )
                        model.counters
            in
            ( { model | counters = newCounters }, Cmd.none )



-- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Counter" ]
        , div []
            (List.indexedMap
                (\ix counter ->
                    Counter.renderCounter counter |> Html.map (CounterMsg ix)
                )
                model.counters
            )
        , hr [] []
        , h1 [] [ text "TreeView" ]
        , div []
            [ TreeView.view model.treeView
            ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
